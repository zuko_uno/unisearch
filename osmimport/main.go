package main

import (
	"fmt"
	"time"
	"unisearch/mgo"
	"unisearch/mgo/bson"
	"unisearch/osmimport/cmap"
)

var (
	node      Node
	um        Umobject
	addr      [5]string
	addr_full string
	cl        *mgo.Collection
	catFlag   int
	err       error
)

type Umobject struct {
	Address  string        "address"
	Email    string        "email"
	Phone    string        "phone"
	Location [2]float64    "location"
	Title    string        "title"
	BestFact string        "best_fact"
	Brief    string        "brief"
	Category bson.ObjectId "category"
}

type Node struct {
	Tags map[string]string "tags"
	Geo  Geometry          "geometry"
}

type Geometry struct {
	Lat    float64 "latitude"
	Lon    float64 "longitude"
	Bottom float64 "bottom"
	Left   float64 "left"
	Top    float64 "top"
	Right  float64 "right"
}

func main() {
	//init db
	session, err := mgo.Dial("localhost")
	if err != nil {
		panic(err)
	}
	session.SetMode(mgo.Monotonic, true)
	cmap.Init()

	cl = session.DB("unimaps").C("umobjects")

	importNodes(session)
	//importWays(session)
}

func importNodes(session *mgo.Session) {

	i := session.DB("backup").C("nodes").
		Find(bson.M{"tags": bson.M{"$exists": true}}).Iter()
	for {
		if !i.Next(&node) {
			break
		}
		parseIt(node)
	}
}

func importWays(session *mgo.Session) {

	i := session.DB("backup").C("ways").
		Find(bson.M{"tags": bson.M{"$exists": true}}).Iter()
	for {
		if !i.Next(&node) {
			break
		}
		parseIt(node)
	}
}

func parseIt(node Node) {
	for k, v := range node.Tags {
		switch {
		case cmap.IsCat(k):
			id := cmap.Map(k, v)
			if id != "" {
				catFlag = 1
				um.Category = bson.ObjectIdHex(id)
			}
		case k == "addr:housenumber":
			addr[4] = v
		case k == "addr:street":
			addr[3] = v
		case k == "addr:place":
			addr[2] = v
		case k == "addr:city":
			addr[1] = v
		case k == "addr:country":
			addr[0] = v
		case k == "operator":
			if um.Title == "" {
				um.Title = v
			}
		case k == "addr:full":
			addr_full = v
		case k == "description":
			if um.Brief != "" {
				um.Brief = v + "\n" + um.Brief
			} else {
				um.Brief = v
			}
		case k == "email" || k == "contact:email":
			um.Email = v
		case k == "phone" || k == "contact:phone":
			um.Phone = v
		case k == "name":
			um.Title = v
		case k == "opening_hours":
			if um.Brief != "" {
				um.Brief += "\n" + v
			} else {
				um.Brief = v
			}
		default:
			//fmt.Println("Skip tag: " + k + "=" + v)
		}
	}

	//address
	if addr_full != "" {
		um.Address = addr_full
	} else {
		if a := makeAddr(addr); a != "" {
			um.Address = a
		}
	}

	if node.Geo.Lat != 0 {
		um.Location[0] = node.Geo.Lat
		um.Location[1] = node.Geo.Lon
	} else {
		um.Location[0] = node.Geo.Bottom + (node.Geo.Top-node.Geo.Bottom)/2
		um.Location[1] = node.Geo.Left + (node.Geo.Right-node.Geo.Left)/2

	}

	if um.Title != "" && um.Location[0] != 0.0 && um.Location[1] != 0.0 && catFlag == 1 {
		err = cl.Insert(um)
		fmt.Println(time.Now().Format(time.Stamp) + ": " + um.Title)

		if err != nil {
			panic(err)
		}
	} else {
	}

	//wipe
	um = Umobject{}
	addr = [5]string{}
	addr_full = ""
	catFlag = 0
}

func makeAddr(a [5]string) (addr string) {
	for _, v := range a[1:5] {
		if v != "" {
			addr += v + " "
		}
	}
	return
}
