package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

var (
	sphinx_id = 1
	skip, m   bool
	err       error
)

func main() {

	fi, err := os.Open("/root/words.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer fi.Close()

	r := bufio.NewReader(fi)

	schemaTemplate :=
		`<?xml version="1.0" encoding="utf-8"?>
		 <sphinx:docset>
		   <sphinx:schema>
				 <sphinx:attr name="word" type="string"/> 
				 <sphinx:attr name="q" type="int"/> 
				 <sphinx:field name="text"/>
			 </sphinx:schema>`

	documentTemplate :=
		`<sphinx:document id="sphinx_id">
		   <word>doc_word</word>
			 <q>doc_q</q>
			 <text>doc_text</text>
		 </sphinx:document>
		 `

	fmt.Print(schemaTemplate)

	dictionary := map[string]int{}

	for {

		query, err := r.ReadString('\n')
		if err != nil {
			break
		}

		query = strings.Trim(query, ".,?!:")
		words := strings.Split(query, " ")

		for _, word := range words {
			word = strings.Trim(word, "\n")
			skip = false

			if len([]rune(word)) <= 4 {
				skip = true
			}

			if m, err = regexp.MatchString("^[0-9]+", word); m {
				skip = true
			}

			if m, err = regexp.MatchString("^.?[0-9]+.?", word); m {
				skip = true
			}

			if !skip {
				quantity, ok := dictionary[word]
				if ok {
					dictionary[word] = quantity + 1
				} else {
					dictionary[word] = 1
				}
			}

			if err != nil {
				fmt.Println(err)
			}

		}
	}

	sphinx_id = 1
	for word, q := range dictionary {
		sphinx_id_s := strconv.Itoa(sphinx_id)
		quantity := strconv.Itoa(q)

		//escaping bad chars
		word = escapeReservedChars(word)

		//print xml-document
		document := documentTemplate
		document = strings.Replace(document, "sphinx_id", sphinx_id_s, -1)
		document = strings.Replace(document, "doc_word", word, -1)
		document = strings.Replace(document, "doc_q", quantity, -1)
		document = strings.Replace(document, "doc_text", word, -1)
		fmt.Print(document)

		sphinx_id += 1
	}

	fmt.Println("</sphinx:docset>")
}

func escapeReservedChars(str string) string {
	str = strings.Replace(str, "&", "&amp;", -1)
	str = strings.Replace(str, "<", "&lt;", -1)
	str = strings.Replace(str, ">", "&gt;", -1)
	str = strings.Replace(str, "'", "&apos;", -1)
	str = strings.Replace(str, "\"", "&quot;", -1)
	return str
}
