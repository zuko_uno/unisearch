package main

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"unisearch/db"
	"unisearch/mgo"
	"unisearch/mgo/bson"
	"unisearch/osmimport/cmap"
)

var (
	geonode                          Response
	node                             Node
	addr                             [5]string
	addr_full, title, text           string
	catFlag, sphinx_id               int
	err                              error
	schemaTemplate, documentTemplate string
	geo_lat, geo_lon, geo_text       string
	bytes                            []byte
)

type Node struct {
	Tags map[string]string "tags"
	Geo  Geometry          "geometry"
}

type Response struct {
	Title, Address string
	Category       CatInfo
	Lat, Lon       float64
}

type CatInfo struct {
	Title, Id string
}

type Geometry struct {
	Lat    float64 "latitude"
	Lon    float64 "longitude"
	Bottom float64 "bottom"
	Left   float64 "left"
	Top    float64 "top"
	Right  float64 "right"
}

func main() {

	session, err := mgo.Dial("localhost")
	if err != nil {
		panic(err)
	}
	session.SetMode(mgo.Monotonic, true)
	cmap.Init()

	schemaTemplate =
		`<?xml version="1.0" encoding="utf-8"?>
		 <sphinx:docset>
		   <sphinx:schema>
			   <sphinx:attr name="longitude" type="float"/>
				 <sphinx:attr name="latitude" type="float"/>
				 <sphinx:attr name="response" type="string"/> 
				 <sphinx:field name="text"/>
			 </sphinx:schema>`

	documentTemplate =
		`<sphinx:document id="sphinx_id">
		   <latitude>geo_lat</latitude>
			 <longitude>geo_lon</longitude>
			 <text>geo_text</text>
			 <response>geo_response</response>
		 </sphinx:document>`

	fmt.Print(schemaTemplate)

	importNodes(session)
	importWays(session)

	fmt.Print("</sphinx:docset>")

	if err != nil {
		panic(err)
	}

}

func importNodes(session *mgo.Session) {

	i := session.DB("osm").C("nodes").
		Find(bson.M{"tags": bson.M{"$exists": true}}).Iter()
	for {
		if !i.Next(&node) {
			break
		}
		parseIt(node)
	}
}

func importWays(session *mgo.Session) {

	i := session.DB("osm").C("ways").
		Find(bson.M{"tags": bson.M{"$exists": true}}).Limit(500).Iter()
	for {
		if !i.Next(&node) {
			break
		}
		parseIt(node)
	}
}

func parseIt(node Node) {
	for k, v := range node.Tags {
		switch {
		case cmap.IsCat(k):
			id := cmap.Map(k, v)
			if id != "" {
				geonode.Category.Id = id
				geonode.Category.Title = cmap.MapTitle(k, v)
			}
		case k == "addr:housenumber":
			addr[4] = v
		case k == "addr:street":
			addr[3] = v
		case k == "addr:place":
			addr[2] = v
		case k == "addr:city":
			addr[1] = v
		case k == "addr:country":
			addr[0] = v
		case k == "operator":
			if geonode.Title == "" {
				geonode.Title = v
				text = text + v + " "
			}
		case k == "addr:full":
			addr_full = v
		case k == "description":
			text = text + v + " "
		case k == "name":
			geonode.Title = v
			text = text + v + " "
		default:
		}
	}

	//address
	if addr_full != "" {
		geonode.Address = addr_full
	} else {
		if a := makeAddr(addr); a != "" {
			geonode.Address = a
			text = a + " " + text
		}
	}

	if node.Geo.Lat != 0 {
		geonode.Lat = node.Geo.Lat
		geonode.Lon = node.Geo.Lon
	} else {
		geonode.Lat = node.Geo.Bottom + (node.Geo.Top-node.Geo.Bottom)/2
		geonode.Lon = node.Geo.Left + (node.Geo.Right-node.Geo.Left)/2
	}

	if (geonode.Title != "" || geonode.Address != "") && geonode.Lat != 0.0 {

		sphinx_id += 1
		geo_lat = strconv.FormatFloat(db.Deg2Rad(geonode.Lat), 'f', -1, 64)
		geo_lon = strconv.FormatFloat(db.Deg2Rad(geonode.Lon), 'f', -1, 64)
		sphinx_id_s := strconv.Itoa(sphinx_id)

		//serialized response object
		bytes, err = json.Marshal(geonode)
		geo_response := string(bytes)

		//escaping bad chars
		geo_text = escapeReservedChars(text)
		geo_response = escapeReservedChars(geo_response)

		//print xml-document
		document := documentTemplate
		document = strings.Replace(document, "sphinx_id", sphinx_id_s, -1)
		document = strings.Replace(document, "geo_lat", geo_lat, -1)
		document = strings.Replace(document, "geo_lon", geo_lon, -1)
		document = strings.Replace(document, "geo_text", geo_text, -1)
		document = strings.Replace(document, "geo_response", geo_response, -1)
		fmt.Print(document)

		if err != nil {
			panic(err)
		}

	}

	//wipe
	geonode = Response{}
	addr = [5]string{}
	addr_full = ""
	text = ""

}

func makeAddr(a [5]string) (addr string) {
	for _, v := range a[1:5] {
		if v != "" {
			addr += v + " "
		}
	}
	return
}

func escapeReservedChars(str string) string {
	str = strings.Replace(str, "&", "&amp;", -1)
	str = strings.Replace(str, "<", "&lt;", -1)
	str = strings.Replace(str, ">", "&gt;", -1)
	str = strings.Replace(str, "'", "&apos;", -1)
	str = strings.Replace(str, "\"", "&quot;", -1)
	return str
}
