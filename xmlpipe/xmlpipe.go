package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"strconv"
	"strings"
	"time"
	"unisearch/db"
	"unisearch/mgo"
)

var (
	target *string = flag.String("t", "objects", "target name")
	volume *string = flag.String("v", "all", "volume: all | delta")
)

func main() {
	flag.Parse()

	//init db
	session, err := mgo.Dial("localhost")
	if err != nil {
		panic(err)
	}
	session.SetMode(mgo.Monotonic, true)
	db.Init(session)

	switch *target {
	case "objects":
		objectsXml()
	case "articles":
		articlesXml()
	case "media":
		mediaXml()
	case "reviews":
		reviewsXml()
	case "questions":
		questionsXml()
	case "notes":
		notesXml()
	case "conferences":
		conferencesXml()
	}
}

func genMiletime() int64 {
	return time.Now().Unix() - 2*60*60*time.Second
}

func objectsXml() {
	var (
		object db.Umobject
		i      *mgo.Iter
		err    error
		bytes  []byte
	)
	schemaTemplate :=
		`<?xml version="1.0" encoding="utf-8"?>
	   <sphinx:docset>
	     <sphinx:schema>
		     <sphinx:attr name="longitude" type="float"/>
			  <sphinx:attr name="latitude" type="float"/>
			  <sphinx:attr name="rating" type="int"/>
			  <sphinx:attr name="date" type="timestamp"/>
      	  <sphinx:attr name="response" type="string"/> 
			  <sphinx:field name="text"/>
		  </sphinx:schema>`

	documentTemplate :=
		`<sphinx:document id="sphinx_id">
	   	<latitude>obj_lat</latitude>
  		 	<longitude>obj_lon</longitude>
  		 	<rating>obj_rating</rating>
  		 	<date>obj_date</date>
  		 	<text>obj_text</text>
  		 	<response>obj_response</response>
  	 	 </sphinx:document>`

	//print schema
	fmt.Print(schemaTemplate)

	//specify data
	switch *volume {
	case "all":
		i = db.Umobjects()
	case "delta":
		i = db.UmobjectsSince(genMiletime())
	}

	for {
		if !i.Next(&object) {
			break
		}
		sphinx_id := strconv.FormatUint(object.Id.SphinxId(), 10)
		obj_lat := strconv.FormatFloat(db.Deg2Rad(object.Location[0]), 'f', -1, 64)
		obj_lon := strconv.FormatFloat(db.Deg2Rad(object.Location[1]), 'f', -1, 64)
		obj_date := strconv.FormatInt(object.LastEdit.Time)
		obj_text := object.FullText()
		obj_rating := object.Rating

		//serialized search response object
		bytes, err = json.Marshal(object.ResponseObject())
		obj_response := string(bytes)

		//escaping bad chars
		obj_text = escapeReservedChars(obj_text)
		obj_response = escapeReservedChars(obj_response)

		//print xml-document
		document := documentTemplate
		document = strings.Replace(document, "sphinx_id", sphinx_id, -1)
		document = strings.Replace(document, "obj_lat", obj_lat, -1)
		document = strings.Replace(document, "obj_lon", obj_lon, -1)
		document = strings.Replace(document, "obj_rating", strconv.Itoa(obj_rating), -1)
		document = strings.Replace(document, "obj_date", obj_date, -1)
		document = strings.Replace(document, "obj_text", obj_text, -1)
		document = strings.Replace(document, "obj_response", obj_response, -1)
		fmt.Print(document)

		err = i.Err()
	}

	fmt.Print("</sphinx:docset>")
	if err != nil {
		panic(err)
	}
}

func articlesXml() {
	var (
		article db.Article
		i       *mgo.Iter
		err     error
		bytes   []byte
	)

	schemaTemplate :=
		`<?xml version="1.0" encoding="utf-8"?>
	   	<sphinx:docset>
	   	<sphinx:schema>
		   	<sphinx:attr name="response" type="string"/>
				<sphinx:attr name="date" type="timestamp"/>
				<sphinx:attr name="rating" type="int"/>
				<sphinx:field name="base"/>
			</sphinx:schema>`

	documentTemplate :=
		`<sphinx:document id="sphinx_id">
	     	<response>art_response</response>
			<date>art_date</date>
			<rating>obj_rating</rating>
			<base>article_base</base>
		 </sphinx:document>`

	//print schema
	fmt.Print(schemaTemplate)

	//specify data
	switch *volume {
	case "all":
		i = db.Articles()
	case "delta":
		i = db.ArticlesSince(genMiletime())
	}

	for {
		if !i.Next(&article) {
			break
		}

		object := article.Object()

		bytes, err = json.Marshal(article.ResponseArticle())
		art_response := string(bytes)
		art_date := strconv.FormatUint(article.LastEdit.Time, 10)

		sphinx_id := strconv.FormatUint(object.Id.SphinxId(), 10)
		obj_rating := strconv.Itoa(object.Rating)

		article_base := article.FullText()

		//escaping bad chars
		article_base = escapeReservedChars(article_base)
		art_response = escapeReservedChars(art_response)

		//print xml-document
		document := documentTemplate
		document = strings.Replace(document, "sphinx_id", sphinx_id, -1)
		document = strings.Replace(document, "art_response", art_response, -1)
		document = strings.Replace(document, "art_date", art_date, -1)
		document = strings.Replace(document, "obj_rating", obj_rating, -1)
		document = strings.Replace(document, "article_base", article_base, -1)
		fmt.Print(document)

		err = i.Err()
	}
	fmt.Print("</sphinx:docset>")
	if err != nil {
		panic(err)
	}
}

func reviewsXml() {
	var (
		review db.Review
		i      *mgo.Iter
		err    error
		bytes  []byte
	)
	//load templates
	schemaTemplate :=
		`<?xml version="1.0" encoding="utf-8"?>
		 <sphinx:docset>
	     	<sphinx:schema>
		     	<sphinx:attr name="rating" type="int"/>
				<sphinx:attr name="response" type="string"/>
		     	<sphinx:attr name="date" type="timestamp"/>
				<sphinx:field name="base"/> 
			</sphinx:schema>`

	documentTemplate :=
		`<sphinx:document id="sphinx_id">
	     	<rating>obj_rating</rating>
			<response>review_response</response>
			<date>review_date</date>
			<base>review_base</base>
		</sphinx:document>`

	//print schema
	fmt.Print(schemaTemplate)

	//specify data
	switch *volume {
	case "all":
		i = db.Reviews()
	case "delta":
		i = db.ReviewsSince(genMiletime())
	}

	for {
		if !i.Next(&review) {
			break
		}

		bytes, err = json.Marshal(review.ResponseReview())
		review_response := string(bytes)

		object := review.Object()
		sphinx_id := strconv.FormatUint(review.Id.SphinxId(), 10)
		obj_rating := strconv.Itoa(object.Rating)
		review_date := strconv.FormatUint(object.LastEdit.Time, 10)
		review_base := review.FullText()

		//escaping bad chars
		review_base = escapeReservedChars(review_base)
		review_response = escapeReservedChars(review_response)

		//print xml-document
		document := documentTemplate
		document = strings.Replace(document, "sphinx_id", sphinx_id, -1)
		document = strings.Replace(document, "review_response", review_response, -1)
		document = strings.Replace(document, "obj_rating", obj_rating, -1)
		document = strings.Replace(document, "review_base", review_base, -1)
		document = strings.Replace(document, "review_date", strconv.Itoa(obj_rating), -1)
		fmt.Print(document)

		err = i.Err()
	}
	fmt.Print("</sphinx:docset>")
	if err != nil {
		panic(err)
	}
}

func mediaXml() {
	var (
		object db.Umobject
		media  db.Media
		i      *mgo.Iter
		bytes  []byte
		err    error
	)
	schemaTemplate :=
		`<?xml version="1.0" encoding="utf-8"?>
     	<sphinx:docset>
      	<sphinx:schema>
         	<sphinx:attr name="response" type="string"/>
     	  	<sphinx:attr name="type" type="string"/>
     	   	<sphinx:attr name="parent" type="string"/>
     	   	<sphinx:attr name="rating" type="int"/>
     	   	<sphinx:attr name="date" type="timestamp"/>
     	   	<sphinx:field name="base"/>
      	</sphinx:schema>`

	documentTemplate :=
		`<sphinx:document id="sphinx_id">
	     <response>media_response</response>
	     <type>media_type</type>
	     <rating>media_rating</rating>
	     <date>media_date</date>
	     <base>media_base</base>
     </sphinx:document>`

	//print schema
	fmt.Print(schemaTemplate)

	//specify data
	switch *volume {
	case "all":
		i = db.Media()
	case "delta":
		i = db.MediaSince(genMiletime())
	}
	mTime := genMiletime()

	for {
		if !i.Next(&media) {
			break
		}

		//go through videos
		for _, video := range object.Videos {
			if (*volume == "all") || (video.LastEdit.Time > mTime) {
				sphinx_id := strconv.FormatUint(video.Id.SphinxId(), 10)
				media_base := video.FullText(object)
				media_rating := strconv.Itoa(video.Rating)
				video.Type = "video"

				//response struct
				bytes, err = json.Marshal(video.ResponseMedia(object))
				media_response := string(bytes)

				//escaping bad chars
				media_base = escapeReservedChars(media_base)
				media_response = escapeReservedChars(media_response)

				//print xml
				document := documentTemplate
				document = strings.Replace(document, "sphinx_id", sphinx_id, -1)
				document = strings.Replace(document, "media_response", media_response, -1)
				document = strings.Replace(document, "media_type", "video", -1)
				document = strings.Replace(document, "media_rating", media_rating, -1)
				document = strings.Replace(document, "media_base", media_base, -1)
				fmt.Print(document)
			}
		}
		err = i.Err()
	}
	fmt.Print("</sphinx:docset>")
	if err != nil {
		panic(err)
	}
}

func questionsXml() {
	var (
		q     db.Question
		i     *mgo.Iter
		bytes []byte
		err   error
	)

	schemaTemplate :=
		`<?xml version="1.0" encoding="utf-8"?>
	 <sphinx:docset>
	   <sphinx:schema>
			 <sphinx:attr name="response" type="string"/>
			 <sphinx:attr name="rating" type="int"/>
			 <sphinx:field name="base"/>
		 </sphinx:schema>`

	documentTemplate :=
		`<sphinx:document id="sphinx_id">
		 <response>q_response</response>
		 <rating>q_rating</rating>
		 <base>q_base</base>
	 </sphinx:document>`

	//print schema
	fmt.Print(schemaTemplate)

	//specify data
	switch *volume {
	case "all":
		i = db.Questions()
	case "delta":
		i = db.QuestionsSince(genMiletime())
	}

	for {
		if !i.Next(&q) {
			break
		}

		bytes, err = json.Marshal(q.ResponseQuestion())
		q_response := string(bytes)

		sphinx_id := strconv.FormatUint(q.Id.SphinxId(), 10)
		q_rating := q.Rating
		q_base := q.FullText()

		//escaping bad chars
		q_base = escapeReservedChars(q_base)
		q_response = escapeReservedChars(q_response)

		//print xml-document
		document := documentTemplate
		document = strings.Replace(document, "sphinx_id", sphinx_id, -1)
		document = strings.Replace(document, "q_response", q_response, -1)
		document = strings.Replace(document, "q_rating", strconv.Itoa(q_rating), -1)
		document = strings.Replace(document, "q_base", q_base, -1)
		fmt.Print(document)

		err = i.Err()
	}
	fmt.Print("</sphinx:docset>")
	if err != nil {
		panic(err)
	}
}

func notesXml() {
	var (
		n     db.Note
		i     *mgo.Iter
		bytes []byte
		err   error
	)

	schemaTemplate :=
		`<?xml version="1.0" encoding="utf-8"?>
	 <sphinx:docset>
	   <sphinx:schema>
			 <sphinx:attr name="response" type="string"/>
			 <sphinx:attr name="rating" type="int"/>
			 <sphinx:field name="base"/>
		 </sphinx:schema>`

	documentTemplate :=
		`<sphinx:document id="sphinx_id">
		 <response>n_response</response>
		 <rating>n_rating</rating>
		 <base>n_base</base>
	 </sphinx:document>`

	//print schema
	fmt.Print(schemaTemplate)

	//specify data
	switch *volume {
	case "all":
		i = db.Notes()
	case "delta":
		i = db.NotesSince(genMiletime())
	}

	for {
		if !i.Next(&n) {
			break
		}

		bytes, err = json.Marshal(n.ResponseNote())
		n_response := string(bytes)

		sphinx_id := strconv.FormatUint(n.Id.SphinxId(), 10)
		n_rating := n.Rating
		n_base := n.FullText()

		//escaping bad chars
		n_base = escapeReservedChars(n_base)
		n_response = escapeReservedChars(n_response)

		//print xml-document
		document := documentTemplate
		document = strings.Replace(document, "sphinx_id", sphinx_id, -1)
		document = strings.Replace(document, "n_response", n_response, -1)
		document = strings.Replace(document, "n_rating", strconv.Itoa(n_rating), -1)
		document = strings.Replace(document, "n_base", n_base, -1)
		fmt.Print(document)

		err = i.Err()
	}
	fmt.Print("</sphinx:docset>")
	if err != nil {
		panic(err)
	}
}

func conferencesXml() {
	var (
		c     db.Conference
		i     *mgo.Iter
		bytes []byte
		err   error
	)

	schemaTemplate :=
		`<?xml version="1.0" encoding="utf-8"?>
	 <sphinx:docset>
	   <sphinx:schema>
			 <sphinx:attr name="response" type="string"/>
			 <sphinx:field name="base"/>
		 </sphinx:schema>`

	documentTemplate :=
		`<sphinx:document id="sphinx_id">
		 <response>c_response</response>
		 <base>c_base</base>
	 </sphinx:document>`

	//print schema
	fmt.Print(schemaTemplate)

	//specify data
	switch *volume {
	case "all":
		i = db.Conferences()
	case "delta":
		i = db.ConferencesSince(genMiletime())
	}

	for {
		if !i.Next(&c) {
			break
		}

		bytes, err = json.Marshal(c.ResponseConference())
		c_response := string(bytes)

		sphinx_id := strconv.FormatUint(c.Id.SphinxId(), 10)
		c_base := c.FullText()

		//escaping bad chars
		c_base = escapeReservedChars(c_base)
		c_response = escapeReservedChars(c_response)

		//print xml-document
		document := documentTemplate
		document = strings.Replace(document, "sphinx_id", sphinx_id, -1)
		document = strings.Replace(document, "c_response", c_response, -1)
		document = strings.Replace(document, "c_base", c_base, -1)
		fmt.Print(document)

		err = i.Err()
	}
	fmt.Print("</sphinx:docset>")
	if err != nil {
		panic(err)
	}
}

func escapeReservedChars(str string) string {
	str = strings.Replace(str, "&", "&amp;", -1)
	str = strings.Replace(str, "<", "&lt;", -1)
	str = strings.Replace(str, ">", "&gt;", -1)
	str = strings.Replace(str, "'", "&apos;", -1)
	str = strings.Replace(str, "\"", "&quot;", -1)
	return str
}
