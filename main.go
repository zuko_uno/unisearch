package main

import (
	"encoding/json"
	"fmt"
	//sphinx "github.com/yunge/gosphinx"
	"net/http"
	"os"
	"runtime"
	"strconv"
	"unisearch/db"
	"unisearch/mgo"
	"unisearch/sphinx"
	//"unisearch/mgo/bson"
)

var (
	sc       *sphinx.SphinxClient
	host     = "localhost"
	port     = 9312
	s        *mgo.Session
	response string
	err      error
)

const (
	pagesize = 5
)

type ObjectsSearch struct {
	Objects  []db.ResponseObject
	Counters map[string]int
	Keywords []string
}

type ArticlesSearch struct {
	Articles []db.ResponseArticle
	Counters map[string]int
	Keywords []string
}

type ReviewsSearch struct {
	Reviews  []db.ResponseReview
	Counters map[string]int
	Keywords []string
}

type MediaSearch struct {
	Media    []db.ResponseMedia
	Counters map[string]int
	Keywords []string
}

type QuestionsSearch struct {
	Questions []db.ResponseQuestion
	Counters  map[string]int
	Keywords  []string
}

type NotesSearch struct {
	Notes    []db.ResponseNote
	Counters map[string]int
	Keywords []string
}

type ConferencesSearch struct {
	Conferences []db.ResponseConference
	Counters    map[string]int
	Keywords    []string
}

type GeocodingSearch struct {
	Nodes []Geonode
}

type Geonode struct {
	Title, Address string
	Category       CatInfo
	Lat, Lon       float64
}

type CatInfo struct {
	Title, Id string
}

type Autocomplete struct {
	Words []string
}

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	//fire up MongoConnection
	s, err = mgo.Dial("localhost")
	s.SetMode(mgo.Monotonic, true)
	http.HandleFunc("/search", search)
	http.ListenAndServe(":8077", nil)
}

func search(w http.ResponseWriter, r *http.Request) {
	var (
		// lat, lon, rad float64
		err error
	)

	//fire up SphinxConnection
	//why sc needed to be initialized in that thread, not main?
	sc = sphinx.NewSphinxClient()
	sc.SetServer(host, port)
	sc.Open()

	//init db, for inner mongo-queries
	db.Init(s)

	//req params
	search_type := r.FormValue("type")
	query := r.FormValue("text")
	page := 1
	if r.FormValue("p") != "" {
		page, err = strconv.Atoi(r.FormValue("p"))
		if err != nil || page == 0 {
			page = 1
		}
	}
	// sorting values: by "date"(desc) and by "rating"(desc)
	sort := r.FormValue("sort")

	/*	if search_type == "geo" {
		lat, err = strconv.ParseFloat(r.FormValue("lat"), 32)
		lon, err = strconv.ParseFloat(r.FormValue("lon"), 32)
		rad, err = strconv.ParseFloat(r.FormValue("rad"), 32)
	}*/

	switch search_type {
	case "objects":
		response = objectsSearch(query, page, sort)
		/*	case "articles":
				response = articlesSearch(query, page)
			case "reviews":
				response = reviewsSearch(query, page)
			case "media":
				response = mediaSearch(query, page)
			case "questions":
				response = questionsSearch(query, page)
			case "notes":
				response = notesSearch(query, page)
			case "conferences":
				response = conferencesSearch(query, page)
			case "geo":
				response = geoSearch(lat, lon, rad, query)*/
	case "autocomplete":
		response = autocomplete(query)
	}

	if err != nil {
		panic(err)
	}

	fmt.Fprint(w, response)
}

func objectsSearch(q string, p int, sort string) (response string) {
	saveQuery(q)
	/*	var rObject db.ResponseObject

		respStruct := new(ObjectsSearch)

		//pagination
		sc.SetLimits((p-1)*pagesize, pagesize, 200, 0)
		if sort != "" {
			sc.SetSortMode(sphinx.SPH_SORT_ATTR_DESC, sort)
		}
		serp, err := sc.Query(q, "objects", "objects search: "+q)

		for _, match := range serp.Matches {
			serializedResponse := match.AttrValues[4].([]byte)
			err = json.Unmarshal(serializedResponse, &rObject)
			respStruct.Objects = append(respStruct.Objects, rObject)
			if err != nil {
				panic(err)
			}
		}

		respStruct.Counters = getCounters(q)
		respStruct.Keywords = getKeywords(q, "objects_delta")

		respStruct = new(ObjectsSearch) //remove it, when finish that method, ZAGLUSHKA BLYA
		bytes, err := json.Marshal(respStruct)
		response = string(bytes)

		if err != nil {
			panic(err)
		}*/
	return "zaglushka, but i save your request BWHAHAHAAHA"
}

/*
func articlesSearch(q string, p int) (response string) {
	var (
		rArticle    db.ResponseArticle
		docs        [20]string
		article_ids []bson.ObjectId
		article     db.Article
	)

	respStruct := new(ArticlesSearch)

	//pagination
	sc.SetLimits((p-1)*pagesize, pagesize, 200, 0)
	serp, err := sc.Query(q, "articles", "articles search: "+q)

	if serp.TotalFound > 0 {
		for _, match := range serp.Matches {
			serializedResponse := match.AttrValues[0].([]byte)
			err = json.Unmarshal(serializedResponse, &rArticle)
			respStruct.Articles = append(respStruct.Articles, rArticle)
			article_ids = append(article_ids, bson.ObjectIdHex(rArticle.Id))
			if err != nil {
				panic(err)
			}
		}

		//fetch articles from mongo and fill docs array
		iArticles := s.DB("unimaps").C("umobjects.articles").
			Find(bson.M{"_id": bson.M{"$in": article_ids}}).Iter()
		for {
			if !iArticles.Next(&article) {
				break
			}
			for i, rArticle := range respStruct.Articles {
				if rArticle.Id == article.Id.Hex() {
					docs[i] = article.FullText()
				}
			}
		}

		//fill quotes in response struct
		excerpts := getExcerpts(docs[:], "articles_delta", q)
		for i := range respStruct.Articles {
			respStruct.Articles[i].Quote = excerpts[i]
		}
	}

	respStruct.Counters = getCounters(q)
	respStruct.Keywords = getKeywords(q, "articles_delta")
	bytes, err := json.Marshal(respStruct)
	response = string(bytes)

	if err != nil {
		panic(err)
	}

	return
}

func reviewsSearch(q string, p int) (response string) {
	var (
		rReview    db.ResponseReview
		docs       []string
		review_ids []bson.ObjectId
		review     db.Review
	)

	respStruct := new(ReviewsSearch)

	//pagination
	sc.SetLimits((p-1)*pagesize, pagesize, 200, 0)
	serp, err := sc.Query(q, "reviews", "reviews search: "+q)

	if serp.TotalFound > 0 {
		for _, match := range serp.Matches {
			serializedResponse := match.AttrValues[1].([]byte)
			err = json.Unmarshal(serializedResponse, &rReview)
			respStruct.Reviews = append(respStruct.Reviews, rReview)
			review_ids = append(review_ids, bson.ObjectIdHex(rReview.Id))
			if err != nil {
				panic(err)
			}
		}

		//fetch reviews from mongo and fill docs array
		iReviews := s.DB("unimaps").C("community.reviews").
			Find(bson.M{"_id": bson.M{"$in": review_ids}}).Iter()
		for {
			if !iReviews.Next(&review) {
				break
			}
			for _, rReview := range respStruct.Reviews {
				if rReview.Id == review.Id.Hex() {
					docs = append(docs, review.FullText())
				}
			}
		}

		//fill quotes in response struct
		excerpts := getExcerpts(docs, "reviews_delta", q)
		for i := range respStruct.Reviews {
			respStruct.Reviews[i].Quote = excerpts[i]
		}
	}

	respStruct.Counters = getCounters(q)
	respStruct.Keywords = getKeywords(q, "reviews_delta")

	bytes, err := json.Marshal(respStruct)
	response = string(bytes)

	if err != nil {
		panic(err)
	}

	return
}

func mediaSearch(q string) (response string) {
	var rMedia db.ResponseMedia

	respStruct := new(MediaSearch)
	serp, err := sc.Query(q, "media", "media search: "+q)

	for _, match := range serp.Matches {
		serializedResponse := match.AttrValues[0].([]byte)
		err = json.Unmarshal(serializedResponse, &rMedia)
		respStruct.Media = append(respStruct.Media, rMedia)
		if err != nil {
			panic(err)
		}
	}

	respStruct.Counters = getCounters(q)
	bytes, err := json.Marshal(respStruct)
	response = string(bytes)

	if err != nil {
		panic(err)
	}
	return
}

func questionsSearch(q string) (response string) {
	var (
		rQuestion    db.ResponseQuestion
		docs         []string
		question_ids []bson.ObjectId
		question     db.Question
	)

	respStruct := new(QuestionsSearch)
	serp, err := sc.Query(q, "questions", "questions search: "+q)

	if serp.TotalFound > 0 {
		for _, match := range serp.Matches {
			serializedResponse := match.AttrValues[0].([]byte)
			err = json.Unmarshal(serializedResponse, &rQuestion)
			respStruct.Questions = append(respStruct.Questions, rQuestion)
			question_ids = append(question_ids, bson.ObjectIdHex(rQuestion.Id))
			if err != nil {
				panic(err)
			}
		}

		//fetch questions from mongo and fill docs array
		iQuestions := s.DB("unimaps").C("community.questions").
			Find(bson.M{"_id": bson.M{"$in": question_ids}}).Iter()
		for {
			if !iQuestions.Next(&question) {
				break
			}
			for _, rQuestion := range respStruct.Questions {
				if rQuestion.Id == question.Id.Hex() {
					docs = append(docs, question.FullText())
				}
			}
		}

		//fill quotes in response struct
		excerpts := getExcerpts(docs, "questions_delta", q)
		for i := range respStruct.Questions {
			respStruct.Questions[i].Quote = excerpts[i]
		}
	}
	respStruct.Counters = getCounters(q)
	bytes, err := json.Marshal(respStruct)
	response = string(bytes)

	if err != nil {
		panic(err)
	}

	return
}

func notesSearch(q string) (response string) {
	var (
		rNote    db.ResponseNote
		docs     []string
		note_ids []bson.ObjectId
		note     db.Note
	)

	respStruct := new(NotesSearch)
	serp, err := sc.Query(q, "notes", "notes search: "+q)

	if serp.TotalFound > 0 {
		for _, match := range serp.Matches {
			serializedResponse := match.AttrValues[0].([]byte)
			err = json.Unmarshal(serializedResponse, &rNote)
			respStruct.Notes = append(respStruct.Notes, rNote)
			note_ids = append(note_ids, bson.ObjectIdHex(rNote.Id))
			if err != nil {
				panic(err)
			}
		}

		//fetch notes from mongo and fill docs array
		iNotes := s.DB("unimaps").C("community.notes").
			Find(bson.M{"_id": bson.M{"$in": note_ids}}).Iter()
		for {
			if !iNotes.Next(&note) {
				break
			}
			for _, rNote := range respStruct.Notes {
				if rNote.Id == note.Id.Hex() {
					docs = append(docs, note.FullText())
				}
			}
		}

		//fill quotes in response struct
		excerpts := getExcerpts(docs, "notes_delta", q)
		for i := range respStruct.Notes {
			respStruct.Notes[i].Quote = excerpts[i]
		}
	}
	respStruct.Counters = getCounters(q)
	bytes, err := json.Marshal(respStruct)
	response = string(bytes)

	if err != nil {
		panic(err)
	}

	return
}

func conferencesSearch(q string, p int) (response string) {
	var (
		rConf    db.ResponseConference
		docs     []string
		conf_ids []bson.ObjectId
		conf     db.Conference
	)

	respStruct := new(ConferencesSearch)

	//pagination
	sc.SetLimits((p-1)*pagesize, pagesize, 200, 0)
	serp, err := sc.Query(q, "conferences", "conferences search: "+q)

	if serp.TotalFound > 0 {
		for _, match := range serp.Matches {
			serializedResponse := match.AttrValues[0].([]byte)
			err = json.Unmarshal(serializedResponse, &rConf)
			respStruct.Conferences = append(respStruct.Conferences, rConf)
			conf_ids = append(conf_ids, bson.ObjectIdHex(rConf.Id))
			if err != nil {
				panic(err)
			}
		}

		//fetch notes from mongo and fill docs array
		iConfs := s.DB("unimaps").C("community.conferences").
			Find(bson.M{"_id": bson.M{"$in": conf_ids}}).Iter()
		for {
			if !iConfs.Next(&conf) {
				break
			}
			for _, rConf := range respStruct.Conferences {
				if rConf.Id == conf.Id.Hex() {
					docs = append(docs, conf.FullText())
				}
			}
		}

		//fill quotes in response struct
		excerpts := getExcerpts(docs, "conferences_delta", q)
		for i := range respStruct.Conferences {
			respStruct.Conferences[i].Quote = excerpts[i]
		}
	}
	respStruct.Counters = getCounters(q)
	respStruct.Keywords = getKeywords(q, "conferences_delta")
	bytes, err := json.Marshal(respStruct)
	response = string(bytes)

	if err != nil {
		panic(err)
	}

	return
}*/

func getCounters(q string) (counters map[string]int) {
	counters = map[string]int{
		"objects":     0,
		"articles":    0,
		"media":       0,
		"questions":   0,
		"reviews":     0,
		"notes":       0,
		"conferences": 0,
	}
	sc.SetLimits(0, 1, 100, 0)
	serp, err := sc.Query(q, "objects", "Objects search, baby!")
	counters["objects"] = serp.Total
	serp, err = sc.Query(q, "articles", "Articles search, baby!")
	counters["articles"] = serp.Total
	serp, err = sc.Query(q, "media", "Media search, baby!")
	counters["media"] = serp.Total
	serp, err = sc.Query(q, "questions", "Questions search, baby!")
	counters["questions"] = serp.Total
	serp, err = sc.Query(q, "notes", "Notes search, baby!")
	counters["notes"] = serp.Total
	serp, err = sc.Query(q, "reviews", "Reviews search, baby!")
	counters["reviews"] = serp.Total
	serp, err = sc.Query(q, "conferences", "Conferences search, baby!")
	counters["conferences"] = serp.Total
	if err != nil {
		panic(err)
	}
	return
}

/*
func geoSearch(lat float64, lon float64, rad float64, q string) (response string) {
	var rGeonode Geonode

	sc.SetGeoAnchor("latitude", "longitude",
		float32(db.Deg2Rad(lat)), float32(db.Deg2Rad(lon)))

	//sc.SetSortMode(sphinx.SPH_SORT_EXTENDED, "@geodist asc")
	sc.SetMatchMode(sphinx.SPH_MATCH_ALL)
	sc.SetFilterFloatRange("@geodist", 0.0, float32(rad), false)

	serp, err := sc.Query("", "geocoding", "")
	sc.ResetFilters()

	respStruct := new(GeocodingSearch)
	for _, match := range serp.Matches {
		serializedResponse := match.AttrValues[2].([]byte)
		err = json.Unmarshal(serializedResponse, &rGeonode)
		respStruct.Nodes = append(respStruct.Nodes, rGeonode)
		if err != nil {
			panic(err)
		}
	}

	bytes, err := json.Marshal(respStruct)
	response = string(bytes)

	if err != nil {
		panic(err)
	}

	return
}

*/

func saveQuery(q string) {
	q = q + "\n"
	f, err := os.OpenFile("/root/words.txt", os.O_APPEND|os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		fmt.Println(err)
		return
	}
	n, err := f.WriteString(q)
	if err != nil {
		fmt.Println(n, err)
		return
	}
	f.Close()
}

func autocomplete(q string) (response string) {
	sc.SetSortMode(sphinx.SPH_SORT_ATTR_DESC, "q")
	serp, err := sc.Query(q+"*", "autocomplete", "autocomplete search")
	sc.ResetFilters()

	respStruct := new(Autocomplete)
	for _, match := range serp.Matches {
		word := string(match.AttrValues[0].([]byte))
		respStruct.Words = append(respStruct.Words, word)
		if err != nil {
			panic(err)
		}
	}

	bytes, err := json.Marshal(respStruct)
	response = string(bytes)

	if err != nil {
		panic(err)
	}

	return
}

func getExcerpts(docs []string, index string, q string) []string {
	opts := sphinx.ExcerptsOpts{
		BeforeMatch:    "<span style='font-weight:bold;color:red'>",
		AfterMatch:     "</span>",
		ChunkSeparator: "..",
		Limit:          140,
		Around:         5,
	}

	res, err := sc.BuildExcerpts(docs, index, q, opts)
	if err != nil {
		panic(err)
	}

	return res
}

func getKeywords(q string, index string) (keywords []string) {

	res, err := sc.BuildKeywords(q, index, true)
	if err != nil {
		panic(err)
	}
	for _, keyword := range res {
		keywords = append(keywords, keyword.Normalized)
	}
	return
}
