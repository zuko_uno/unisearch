package db

import (
	"unisearch/mgo"
	"unisearch/mgo/bson"
)

type Umobject struct {
	Id       bson.ObjectId   "_id"
	Origin   bson.ObjectId   "origin"
	Address  string          "address"
	Email    string          "email"
	Phone    string          "phone"
	Rating   int             "rating"
	Votes    int             "votes"
	Location [2]float64      "location"
	LastEdit EditInfo        "last_edit"
	Title    string          "title"
	BestFact string          "best_fact"
	Brief    string          "brief"
	Category bson.ObjectId   "category"
	Parents  []bson.ObjectId "_parent"
	Tags     []string        "tags"
	Language string          "language"
}

func (o Umobject) CompileUmobject() (um Umobject) {
	if o.Origin.Valid() {
		s.DB("unimaps").C("umobjects").
			Find(bson.M{"_id": o.Origin}).One(&um)
		if o.Address != "" {
			um.Address = o.Address
		}
		if o.Email != "" {
			um.Email = o.Email
		}
		if o.Title != "" {
			um.Title = o.Title
		}
		if o.BestFact != "" {
			um.BestFact = o.BestFact
		}
		if o.Brief != "" {
			um.Brief = o.Brief
		}
		um.Id = o.Id
		um.Language = o.Language
		um.LastEdit = o.LastEdit
	} else {
		um = o
	}
	return
}

func (o Umobject) ResponseObject() (rObject ResponseObject) {
	var (
		obj Umobject
		cat Category
	)

	o = o.CompileUmobject()

	rObject.Id = o.Id.Hex()

	//fill parents
	iParents := s.DB("unimaps").C("umobjects").
		Find(bson.M{"_id": bson.M{"$in": o.Parents}}).Iter()
	for {
		if !iParents.Next(&obj) {
			break
		}
		rObject.Parents = append(rObject.Parents,
			ObjParent{Id: obj.Id.Hex(), Title: obj.Title})
	}

	//fill categories
	if o.Category.Valid() {
		s.DB("unimaps").C("categories").Find(bson.M{"_id": o.Category}).One(&cat)
		rObject.Categories = append(rObject.Categories,
			ObjCategory{Id: cat.Id.Hex(), Title: cat.Name})
		if cat.Parent.Valid() {
			s.DB("unimaps").C("categories").Find(bson.M{"_id": cat.Parent}).One(&cat)
			rObject.Categories = append(rObject.Categories,
				ObjCategory{Id: cat.Id.Hex(), Title: cat.Name})
		}
	}

	rObject.Title = o.Title
	rObject.Geo = o.Location
	rObject.Address = o.Address
	rObject.Favorite = 0
	rObject.Marked = 0
	rObject.Votes = o.Votes
	rObject.Rating = o.Rating
	rObject.Tags = o.Tags

	return
}

type ResponseObject struct {
	Id         string
	Parents    []ObjParent
	Categories []ObjCategory
	Title      string
	Geo        [2]float64
	Address    string
	Favorite   int
	Marked     int
	Rating     int
	Votes      int
	Tags       []string
}

type ObjParent struct {
	Id    string
	Title string
}

type ObjCategory struct {
	Id    string
	Title string
}

func Umobjects() (iter *mgo.Iter) {
	iter = s.DB("unimaps").C("umobjects").Find(nil).Iter()
	return
}

func UmobjectsSince(miletime int64) (iter *mgo.Iter) {
	iter = s.DB("unimaps").C("umobjects").
		Find(bson.M{"last_edit.dtunix": bson.M{"$gte": miletime}}).Iter()
	return
}

func (o Umobject) FullText() (text string) {
	text += o.Title + "\n"
	text += o.Address + "\n"
	text += o.CategoriesText() + "\n"
	text += o.BestFact + "\n"
	text += o.Brief + "\n"
	text += o.ParentsText() + "\n"
	text += o.TagsText() + "\n"
	text += o.Article().FullText() + "\n"
	text += o.ReviewsText() + "\n"
	text += o.QuestionsText() + "\n"
	text += o.MediasText() + "\n"
	return
}

func (o Umobject) TagsText() (text string) {
	for _, t := range o.Tags {
		text += t + " "
	}
	return
}

func (o Umobject) CategoriesText() (text string) {
	var category Category
	o = o.CompileUmobject()

	s.DB("unimaps").C("categories").
		Find(bson.M{"_id": o.Category}).One(&category)
	text += category.Name + "\n" + category.Description + "\n"

	if category.Parent.Valid() {
		s.DB("unimaps").C("categories").
			Find(bson.M{"_id": category.Parent}).One(&category)
		text += category.Name + "\n" + category.Description + "\n"
	}
	return
}

func (o Umobject) ParentsText() (text string) {
	var item Umobject
	o = o.CompileUmobject()

	i := s.DB("unimaps").C("umobjects").
		Find(bson.M{"_id": bson.M{"$in": o.Parents}}).Iter()
	for {
		if !i.Next(&item) {
			break
		}
		text += item.Title + "\n"
	}
	return
}

func (o Umobject) Article() (article Article) {
	o = o.CompileUmobject()
	s.DB("unimaps").C("umobjects.articles").
		Find(bson.M{"obj": o.Id, "language": o.Language}).
		One(&article)
	return
}

func (o Umobject) Reviews() (iter *mgo.Iter) {
	if o.Origin.Valid() {
		iter = s.DB("unimaps").C("community.reviews").
			Find(bson.M{"reference": o.Origin}).Iter()
	} else {
		iter = s.DB("unimaps").C("community.reviews").
			Find(bson.M{"reference": o.Id}).Iter()
	}
	return
}

func (o Umobject) Questions() (iter *mgo.Iter) {
	if o.Origin.Valid() {
		iter = s.DB("unimaps").C("community.questions").
			Find(bson.M{"reference": o.Origin}).Iter()
	} else {
		iter = s.DB("unimaps").C("community.questions").
			Find(bson.M{"reference": o.Id}).Iter()
	}
	return
}

func (o Umobject) Medias() (iter *mgo.Iter) {
	o = o.CompileUmobject()
	iter = s.DB("unimaps").C("umobjects.media").
		Find(bson.M{"reference": o.Id}).Iter()
	return
}

func (o Umobject) MediasText() (text string) {
	var m Media
	iter := o.Medias()
	for {
		if !iter.Next(&m) {
			break
		}
		text += m.Description + " "
	}
	return
}

func (o Umobject) QuestionsText() (text string) {
	var question Question
	qIter := o.Questions()
	for {
		if !qIter.Next(&question) {
			break
		}
		text += question.FullText() + "\n"
	}
	return
}

func (o Umobject) ReviewsText() (text string) {
	var review Review
	rIter := o.Reviews()
	for {
		if !rIter.Next(&review) {
			break
		}
		text += review.FullText()
	}
	return
}
