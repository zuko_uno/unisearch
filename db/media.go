package db

import (
	"unisearch/mgo"
	"unisearch/mgo/bson"
)

type Media struct {
	Id          bson.ObjectId "_id"
	Parent      bson.ObjectId "parent"
	Type        string        "type"
	Description string        "description"
	Url         string        "url"
	Rating      int           "rating"
	LastEdit    EditInfo      "last_edit"
	Deleted     int           "deleted"
}

type MediaParent struct {
	Id       string
	Title    string
	Tags     []string
	Geo      [2]float64
	Address  string
	Favorite int
	Marked   int
	Rating   int
}

type ResponseMedia struct {
	Id          string
	Type        string
	Description string
	Source      string
	Rating      int
	Parent      MediaParent
}

func Medias() (iter *mgo.Iter) {
	iter = s.DB("unimaps").C("umobjects.media").Find(nil).Iter()
	return
}

func MediasSince(miletime int64) (iter *mgo.Iter) {
	iter = s.DB("unimaps").C("umobjects.media").
		Find(bson.M{"last_edit.dtunix": bson.M{"$gte": miletime}}).Iter()
	return
}

func (m Media) FullText(parent Umobject) (text string) {
	text += m.Description + " " + parent.Title + " \n" +
		parent.Address + " \n" + parent.Brief
	return
}

func (m Media) ResponseMedia(parent Umobject) (rMedia ResponseMedia) {
	rMedia.Id = m.Id.Hex()
	rMedia.Type = m.Type
	rMedia.Description = m.Description
	rMedia.Rating = m.Rating
	rMedia.Source = m.Url

	rMedia.Parent = MediaParent{
		Id:       parent.Id.Hex(),
		Title:    parent.Title,
		Tags:     parent.Tags,
		Geo:      parent.Location,
		Address:  parent.Address,
		Favorite: 0,
		Marked:   0,
		Rating:   parent.Rating,
	}

	return
}
