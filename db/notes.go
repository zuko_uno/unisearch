package db

import (
	"unisearch/mgo"
	"unisearch/mgo/bson"
)

type Note struct {
	Id         bson.ObjectId "_id"
	Deleted    int           "deleted"
	Rating     int           "rate"
	Visibility int           "visibility"
	LastEdit   EditInfo      "last_edit"
	AuthorName string        "author_name"
	Text       string        "text"
}

type ResponseNote struct {
	Id         string
	Rating     int
	Text       string
	AuthorName string
	Quote      string
}

func (n Note) ResponseNote() (rNote ResponseNote) {
	rNote.Id = n.Id.Hex()
	rNote.Rating = n.Rating
	rNote.Text = n.FullText()
	rNote.AuthorName = n.AuthorName

	return
}

type Comment struct {
	Id         bson.ObjectId "_id"
	Deleted    int           "deleted"
	Rate       int           "rate"
	AuthorName string        "author_name"
	Parent     bson.ObjectId "parent"
	Text       string        "text"
}

func Notes() (iter *mgo.Iter) {
	iter = s.DB("unimaps").C("community.notes").Find(nil).Iter()
	return
}

func NotesSince(miletime int64) (iter *mgo.Iter) {
	iter = s.DB("unimaps").C("community.notes").
		Find(bson.M{"last_edit.dtunix": bson.M{"$gte": miletime}}).Iter()
	return
}

func (n Note) FullText() (result string) {
	var comment Comment

	result += n.Text + "\n"
	comments := n.Comments()
	for {
		if !comments.Next(&comment) {
			break
		}
		result += comment.Text + "\n"
	}
	return
}

func (n Note) Comments() (iter *mgo.Iter) {
	iter = s.DB("unimaps").C("community.notes.comments").
		Find(bson.M{"parent": n.Id}).Iter()
	return
}
