package db

import (
	"unisearch/mgo"
	"unisearch/mgo/bson"
)

/* ===ВОПРОСЫ И ОТВЕТЫ=== */

type Question struct {
	Id         bson.ObjectId "_id"
	Deleted    int           "deleted"
	Rating     int           "rating"
	AuthorName string        "author_name"
	Reference  bson.ObjectId "reference"
	LastEdit   EditInfo      "last_edit"
	Text       string        "text"
}

type ResponseQuestion struct {
	Id         string
	Rating     int
	Text       string
	AuthorName string
	Parent     QuestionParent
	Quote      string
}

func (q Question) ResponseQuestion() (rQuestion ResponseQuestion) {
	var (
		parent Umobject
	)

	rQuestion.Id = q.Id.Hex()
	parent = q.Object()
	rQuestion.Rating = q.Rating

	rQuestion.Parent = QuestionParent{
		Id:       parent.Id.Hex(),
		Title:    parent.Title,
		Tags:     parent.Tags,
		Geo:      parent.Location,
		Address:  parent.Address,
		Favorite: 0,
		Marked:   0,
		Rating:   parent.Rating,
	}

	return
}

type QuestionParent struct {
	Id       string
	Title    string
	Tags     []string
	Geo      [2]float64
	Address  string
	Favorite int
	Marked   int
	Rating   int
}

type Answer struct {
	Id         bson.ObjectId "_id"
	Deleted    int           "deleted"
	Rate       int           "rate"
	AuthorName string        "author_name"
	Parent     bson.ObjectId "parent"
	Text       string        "text"
}

func (q Question) FullText() (result string) {
	var object Umobject
	var answer Answer

	object = q.Object()
	result += object.Title + "\n" + object.BestFact + "\n" + object.Brief + "\n"
	result += q.Text + "\n"
	answers := s.DB("unimaps").C("community.questions.answers").
		Find(bson.M{"parent": q.Id}).Iter()
	for {
		if !answers.Next(&answer) {
			break
		}
		result += answer.Text + "\n"
	}
	return
}

func (q Question) Answers() (iter *mgo.Iter) {
	iter = s.DB("unimaps").C("community.questions.answers").
		Find(bson.M{"parent": q.Id}).Iter()
	return
}

func Questions() (iter *mgo.Iter) {
	iter = s.DB("unimaps").C("community.questions").Find(nil).Iter()
	return
}

func QuestionsSince(miletime int64) (iter *mgo.Iter) {
	iter = s.DB("unimaps").C("community.questions").
		Find(bson.M{"last_edit.dtunix": bson.M{"$gte": miletime}}).Iter()
	return
}

func (q Question) Object() (object Umobject) {
	s.DB("unimaps").C("umobjects").Find(bson.M{"_id": q.Reference}).One(&object)
	return
}
