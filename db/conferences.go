package db

import (
	"unisearch/mgo"
	"unisearch/mgo/bson"
)

type Conference struct {
	Id         bson.ObjectId "_id"
	Parent     bson.ObjectId "parent"
	Deleted    int           "deleted"
	Visibility int           "visibility"
	AuthorName string        "author_name"
	LastEdit   EditInfo      "last_edit"
	Text       string        "text"
}

type ResponseConference struct {
	Id         string
	Text       string
	AuthorName string
	AuthorId   string
	LastEdit   int64
	Quote      string
	Parent     OpParent
}

func (c Conference) Object() (object Umobject) {
	s.DB("unimaps").C("umobjects").Find(bson.M{"_id": c.Parent}).One(&object)
	return
}

func (c Conference) ResponseConference() (rConf ResponseConference) {
	var (
		cat Category
	)
	rConf.Id = c.Id.Hex()
	rConf.Text = c.FullText()
	rConf.AuthorName = c.LastEdit.UserName
	rConf.AuthorId = c.LastEdit.UserId.Hex()
	rConf.LastEdit = c.LastEdit.Time

	//fill parent info
	parent := c.Object()
	rConf.Parent = OpParent{
		Id:       parent.Id.Hex(),
		Title:    parent.Title,
		Tags:     parent.Tags,
		Geo:      parent.Location,
		Address:  parent.Address,
		Favorite: 0,
		Votes:    parent.Votes,
		Marked:   0,
		Rating:   parent.Rating,
	}

	//fill categories
	s.DB("unimaps").C("categories").Find(bson.M{"_id": parent.Category}).One(&cat)
	rConf.Parent.Categories = append(rConf.Parent.Categories,
		ObjCategory{Id: cat.Id.Hex(), Title: cat.Name})
	if cat.Parent.Valid() {
		s.DB("unimaps").C("categories").Find(bson.M{"_id": cat.Parent}).One(&cat)
		rConf.Parent.Categories = append(rConf.Parent.Categories,
			ObjCategory{Id: cat.Id.Hex(), Title: cat.Name})
	}
	return
}

type Message struct {
	Id         bson.ObjectId "_id"
	Deleted    int           "deleted"
	AuthorName string        "author_name"
	Parent     bson.ObjectId "parent"
	Text       string        "text"
}

func (c Conference) FullText() (result string) {
	var message Message

	result += c.Text + "\n"
	messages := c.Messages()
	for {
		if !messages.Next(&message) {
			break
		}
		result += message.Text + "\n"
	}
	return
}

func (c Conference) Messages() (iter *mgo.Iter) {
	iter = s.DB("unimaps").C("community.conferences.messages").
		Find(bson.M{"parent": c.Id}).Iter()
	return
}

func Conferences() (iter *mgo.Iter) {
	iter = s.DB("unimaps").C("community.conferences").Find(nil).Iter()
	return
}

func ConferencesSince(miletime int64) (iter *mgo.Iter) {
	iter = s.DB("unimaps").C("community.conferences").
		Find(bson.M{"last_edit.dtunix": bson.M{"$gte": miletime}}).Iter()
	return
}
