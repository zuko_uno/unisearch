package db

import (
	"unisearch/mgo"
	"unisearch/mgo/bson"
)

type Article struct {
	Id       bson.ObjectId "_id"
	Origin   bson.ObjectId "origin"
	Parent   bson.ObjectId "parent"
	Language string        "language"
	Sections []Section     "sections"
	LastEdit EditInfo      "last_edit"
}

type Section struct {
	Id      bson.ObjectId "_id"
	Text    string        "text"
	Caption string        "caption"
}

func (a Article) CompileArticle() (ar Article) {
	if a.Origin.Valid() {
		s.DB("unimaps").C("umobjects.articles").
			Find(bson.M{"_id": a.Origin}).One(&ar)
		ar.Sections = a.Sections
		ar.Language = a.Language
	} else {
		ar = a
	}
	return
}

func (a Article) ResponseArticle() (rArticle ResponseArticle) {
	var (
		parent Umobject
	)
	a = a.CompileArticle()

	rArticle.Id = a.Id.Hex()
	rArticle.Language = a.Language
	rArticle.Time = a.LastEdit.Time

	parent = a.Object()

	rArticle.Parent = ArtParent{
		Id:       parent.Id.Hex(),
		Title:    parent.Title,
		Tags:     parent.Tags,
		Geo:      parent.Location,
		Address:  parent.Address,
		Favorite: 0,
		Marked:   0,
		Votes:    parent.Votes,
		Rating:   parent.Rating,
	}

	return
}

type ResponseArticle struct {
	Id       string
	Parent   ArtParent
	Quote    string
	Language string
	Time     int64
}

type ArtParent struct {
	Id       string
	Title    string
	Tags     []string
	Geo      [2]float64
	Address  string
	Favorite int
	Marked   int
	Votes    int
	Rating   int
}

func (ar Article) FullText() (result string) {
	for _, t := range ar.Sections {
		result += t.Caption + "\n" + t.Text + "\n\n"
	}
	return
}

func (ar Article) Object() (object Umobject) {
	ar = ar.CompileArticle()
	s.DB("unimaps").C("umobjects").Find(bson.M{"_id": ar.Parent}).One(&object)
	return
}

func Articles() (iter *mgo.Iter) {
	iter = s.DB("unimaps").C("umobjects.articles").Find(nil).Iter()
	return
}

func ArticlesSince(miletime int64) (iter *mgo.Iter) {
	iter = s.DB("unimaps").C("umobjects.articles").
		Find(bson.M{"last_edit.dtunix": bson.M{"$gte": miletime}}).Iter()
	return
}
