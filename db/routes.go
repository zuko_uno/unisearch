package db

import (
	"unisearch/mgo/bson"
)

type Umroute struct {
	Id       bson.ObjectId   "_id"
	Address  string          "address"
	Rating   int             "rating"
	Location [2]float64      "location"
	Title    string          "title"
	BestFact string          "best_fact"
	Brief    string          "brief"
	Category bson.ObjectId   "category"
	Parents  []bson.ObjectId "_parent"
	Tags     []string        "tags"
}
