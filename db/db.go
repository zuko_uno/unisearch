package db

import (
	"math"
	"strings"
	"unisearch/mgo"
	"unisearch/mgo/bson"
)

var (
	s *mgo.Session
)

func Init(session *mgo.Session) {
	s = session
}

func Deg2Rad(deg float64) float64 {
	return (deg * math.Pi / 180)
}

func Rad2Deg(rad float64) float64 {
	return (rad * 180 / math.Pi)
}

type EditInfo struct {
	UserId   bson.ObjectId "user_id"
	UserName string        "user_name"
	Time     int64         "dtunix"
	Date     int64         "dt"
}

type Category struct {
	Id          bson.ObjectId "_id"
	Name        string        "name"
	Parent      bson.ObjectId "parent"
	Description string        "description"
	Deleted     int           "deleted"
}

func escapeReservedChars(str string) string {
	str = strings.Replace(str, "&", "&amp;", -1)
	str = strings.Replace(str, "<", "&lt;", -1)
	str = strings.Replace(str, ">", "&gt;", -1)
	str = strings.Replace(str, "'", "&apos;", -1)
	str = strings.Replace(str, "\"", "&quot;", -1)
	return str
}
