package db

import (
	"unisearch/mgo"
	"unisearch/mgo/bson"
)

type Review struct {
	Id         bson.ObjectId "_id"
	Deleted    int           "deleted"
	Rate       int           "rate"
	AuthorName string        "author_name"
	Reference  bson.ObjectId "reference"
	Positive   ReviewBody    "positive"
	Negative   ReviewBody    "negative"
	LastEdit   EditInfo      "last_edit"
}

type ReviewBody struct {
	Id      bson.ObjectId "_id"
	Deleted int           "deleted"
	Text    string        "text"
}

func (op Review) ResponseReview() (rReview ResponseReview) {
	var (
		parent Umobject
		cat    Category
	)

	rReview.Id = op.Id.Hex()
	rReview.AuthorId = op.LastEdit.UserId.Hex()
	rReview.AuthorName = op.LastEdit.UserName
	rReview.LastEditTime = op.LastEdit.Time

	//fill parent info
	parent = op.Object()
	rReview.Parent = OpParent{
		Id:       parent.Id.Hex(),
		Title:    parent.Title,
		Tags:     parent.Tags,
		Geo:      parent.Location,
		Address:  parent.Address,
		Favorite: 0,
		Votes:    parent.Votes,
		Marked:   0,
		Rating:   parent.Rating,
	}

	//fill categories
	s.DB("unimaps").C("categories").Find(bson.M{"_id": parent.Category}).One(&cat)
	rReview.Parent.Categories = append(rReview.Parent.Categories,
		ObjCategory{Id: cat.Id.Hex(), Title: cat.Name})
	if cat.Parent.Valid() {
		s.DB("unimaps").C("categories").Find(bson.M{"_id": cat.Parent}).One(&cat)
		rReview.Parent.Categories = append(rReview.Parent.Categories,
			ObjCategory{Id: cat.Id.Hex(), Title: cat.Name})
	}

	return
}

type ResponseReview struct {
	Id           string
	Quote        string
	AuthorName   string
	AuthorId     string
	LastEditTime int64
	Parent       OpParent
}

type OpParent struct {
	Id         string
	Title      string
	Tags       []string
	Geo        [2]float64
	Address    string
	Categories []ObjCategory
	Favorite   int
	Marked     int
	Rating     int
	Votes      int
}

func (op Review) FullText() (result string) {
	result += op.Negative.Text + "\n" + op.Positive.Text + "\n\n"
	return
}

func (op Review) Object() (object Umobject) {
	s.DB("unimaps").C("umobjects").Find(bson.M{"_id": op.Reference}).One(&object)
	return
}

func Reviews() (iter *mgo.Iter) {
	iter = s.DB("unimaps").C("community.reviews").Find(nil).Iter()
	return
}

func ReviewsSince(miletime int64) (iter *mgo.Iter) {
	iter = s.DB("unimaps").C("community.reviews").
		Find(bson.M{"last_edit.dtunix": bson.M{"$gte": miletime}}).Iter()
	return
}
